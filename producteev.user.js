// ==UserScript==
// @name          A bit better Producteev
// @namespace     JirkaHronik
// @author        Jirka Hronik <jirka.hronik@ocius.cz>
// @description   Producteev lacks some of the basic functionality that this extension provides.
// @downloadURL   https://bitbucket.org/jirkahronik/chromeextentensions/raw/3c685aeb9bbac1a18659be0e224c064d7f9d0082/producteev.user.js
// @updateURL     https://bitbucket.org/jirkahronik/chromeextentensions/raw/3c685aeb9bbac1a18659be0e224c064d7f9d0082/producteev.user.js
// @version       1.0
// @include       https://dev.2n.cz/*
// ==/UserScript==
/*
Changelog:

*/ 

// Initial bootstrap - include libraries and run callback once all is ready..
function bootstrap(callback) {
  callback();
}

// The guts of this userscript.
function main() {
  // Define custom stylesheet.
  $("<style type='text/css'></style>").text('\
    .late {\
      color: #f00;\
      font-weight: bold;\
    }\
  ').appendTo("head");

  // Set default sorting: by deadline ASC.
  $('a[data-sortbyvalue="deadline_time"]').click();
  $('a[data-orderbyvalue="asc"]').click();
  
  // Mark all the late deadlines.
  $('.deadline-string').each(function(key,item){
    var today = new Date();
    var taskDateParts = $(this).text().split(', ');
    var taskDate = new Date(taskDateParts[1]+' '+today.getFullYear());
    if ((taskDate - today) < (3600*1000)) {
      $(item).addClass('late');
    } 
  });

}

// Load all and execute the main function.
bootstrap(main); 